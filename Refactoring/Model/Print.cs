﻿namespace Refactoring.Model
{
    public class Print
    {
        public Print(int _MaxNumPrimes, int _RowsPerPage, int _ColsPerPage) {
            MaxNumPrimes = _MaxNumPrimes;
            RowsPerPage = _RowsPerPage;
            ColsPerPage = _ColsPerPage;
        }

        public int MaxNumPrimes { get; set; }
        public int RowsPerPage { get; set; }
        public int ColsPerPage { get; set; }
    }
}
