﻿namespace Refactoring.Model
{
    public class PrintSetting
    {
        public PrintSetting()
        {

        }

        public int NextNumber { set; get; }
        public int CurrentPrimeNumber { set; get; }
        public bool IsPrime { set; get; }
        public int Ordinal { set; get; }
        public int Square { set; get; }
    }
}
