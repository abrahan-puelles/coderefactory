﻿namespace Refactoring.Model
{
    public class PageSetting
    {
        public PageSetting() {}
        public int PageNumber { get; set; }
        public int PageOffset { get; set; }
        public int RowOffset { get; set; }
        public int CurrentColumnNum { get; set; }
    }
}
