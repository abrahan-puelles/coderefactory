﻿namespace Refactoring.Common
{
    public class Constants
    {
        public const int MAX_NUM_PRIMES = 1000;
        public const int ROWS_PER_PAGE = 50;
        public const int COLS_PER_PAGE = 4;
        public const int MAX_ORDINAL = 30;
    }
}
