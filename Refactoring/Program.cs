﻿using Refactoring.Common;
using Refactoring.Model;
using System;
using System.Text;

namespace Refactoring
{
    class Program
    {
        static void Main(string[] args)
        {
            var printer = Printer();
            Console.WriteLine(printer.ToString());
        }

        public static string Printer() {
            Print printConfiguration = new Print(Constants.MAX_NUM_PRIMES, Constants.ROWS_PER_PAGE, Constants.COLS_PER_PAGE);
            int[] primes = new int[(printConfiguration.MaxNumPrimes + 1)];
            primes[1] = 2;
            PrintSetting _PrintSetting = new PrintSetting() { NextNumber = 1, CurrentPrimeNumber = 1, IsPrime = false, Ordinal = 2, Square = 9 };
            int[] squares = new int[(Constants.MAX_ORDINAL + 1)];
            while ((_PrintSetting.CurrentPrimeNumber < printConfiguration.MaxNumPrimes))
            {
                while (!_PrintSetting.IsPrime)
                {
                    _PrintSetting.NextNumber += 2;
                    if (_PrintSetting.NextNumber == _PrintSetting.Square)
                    {
                        _PrintSetting.Ordinal += 1;
                        _PrintSetting.Square = primes[_PrintSetting.Ordinal] * primes[_PrintSetting.Ordinal];
                        squares[(_PrintSetting.Ordinal - 1)] = _PrintSetting.NextNumber;
                    }
                    int primeNumber = 2;
                    _PrintSetting.IsPrime = true;
                    while ((primeNumber < _PrintSetting.Ordinal) && _PrintSetting.IsPrime)
                    {
                        while (squares[primeNumber] < _PrintSetting.NextNumber)
                        {
                            squares[primeNumber] = (squares[primeNumber] + (primes[primeNumber] + primes[primeNumber]));
                        }
                        if (squares[primeNumber] == _PrintSetting.NextNumber)
                        {
                            _PrintSetting.IsPrime = false;
                        }
                        primeNumber += 1;
                    }
                }
                _PrintSetting.CurrentPrimeNumber += 1;
                primes[_PrintSetting.CurrentPrimeNumber] = _PrintSetting.NextNumber;
            }

            return PrinterOutput(printConfiguration, primes);
        }
        public static string PrinterOutput(Print print, int[] primes)
        {
            StringBuilder primesOutput = new StringBuilder();
            PageSetting pageSetting = new PageSetting() { PageNumber = 1, PageOffset = 1 };
            while ((pageSetting.PageOffset <= print.MaxNumPrimes))
            {
                primesOutput.Append($"The First {print.MaxNumPrimes} Prime Numbers --- Page {pageSetting.PageNumber}");
                primesOutput.Append("\n");
                for (pageSetting.RowOffset = pageSetting.PageOffset; (pageSetting.RowOffset < (pageSetting.PageOffset + print.RowsPerPage)); pageSetting.RowOffset++)
                {
                    for (pageSetting.CurrentColumnNum = 0; (pageSetting.CurrentColumnNum < print.ColsPerPage); pageSetting.CurrentColumnNum++)
                    {
                        if ((pageSetting.RowOffset + (pageSetting.CurrentColumnNum * print.RowsPerPage)) <= print.MaxNumPrimes)
                        {
                            string offset = primes[(pageSetting.RowOffset
                                            + (pageSetting.CurrentColumnNum * print.RowsPerPage))].ToString("D10");
                            primesOutput.Append(offset);
                        }
                    }
                    primesOutput.Append("\n");
                }
                pageSetting.PageNumber = (pageSetting.PageNumber + 1);
                pageSetting.PageOffset = (pageSetting.PageOffset + (print.RowsPerPage * print.ColsPerPage));
            }

            return primesOutput.ToString();
        }
    }
}